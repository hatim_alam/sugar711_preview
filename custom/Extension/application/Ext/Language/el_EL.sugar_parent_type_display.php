<?php
 // created: 2018-01-23 09:18:09

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Λογαριασμός',
  'Contacts' => 'Επαφή',
  'Tasks' => 'Εργασία',
  'Opportunities' => 'Ευκαιρία',
  'Products' => 'Στοιχείο Γραμμής με Προσφορά',
  'Quotes' => 'Προσφορά',
  'Bugs' => 'Σφάλματα',
  'Cases' => 'Υπόθεση',
  'Leads' => 'Δυνητικός Πελάτης',
  'Project' => 'Έργο',
  'ProjectTask' => 'Εργασία Έργου',
  'Prospects' => 'Στόχος',
  'KBContents' => 'Βάση Γνώσεων',
  'RevenueLineItems' => 'Στοιχεία γραμμής εσόδων',
);