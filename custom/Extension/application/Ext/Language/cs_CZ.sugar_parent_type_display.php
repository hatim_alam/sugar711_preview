<?php
 // created: 2018-01-23 09:18:07

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Společnost',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Úkol',
  'Opportunities' => 'Příležitost',
  'Products' => 'Produkt',
  'Quotes' => 'Nabídka',
  'Bugs' => 'Chyby',
  'Cases' => 'Případ:',
  'Leads' => 'Příležitost',
  'Project' => 'Projekty',
  'ProjectTask' => 'Projektové úkoly',
  'Prospects' => 'Kontakt',
  'KBContents' => 'Znalostní báze',
  'RevenueLineItems' => 'Řádky tržby',
);