<?php
 // created: 2018-01-23 09:18:16

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Persoon',
  'Tasks' => 'Taak',
  'Opportunities' => 'Opportunity',
  'Products' => 'Geoffreerd product',
  'Quotes' => 'Offerte',
  'Bugs' => 'Bugs',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Projecttaak',
  'Prospects' => 'Target',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Opportunityregels',
);