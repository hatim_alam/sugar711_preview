<?php
 // created: 2018-01-23 09:18:22

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Compte',
  'Opportunities' => 'Oportunitat',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Contacts' => 'Contactes',
  'Products' => 'Element de línia d\'oferta',
  'Quotes' => 'Pressupost',
  'Bugs' => 'Incidència',
  'Project' => 'Projecte',
  'Prospects' => 'Objectiu',
  'ProjectTask' => 'Tasca de projecte',
  'Tasks' => 'Tasca',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Elements de línia d\'ingressos',
);