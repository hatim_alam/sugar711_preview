<?php
 // created: 2018-01-23 09:18:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'חשבון',
  'Opportunities' => 'הזדמנות',
  'Cases' => 'פניית שירות',
  'Leads' => 'ליד',
  'Contacts' => 'אנשי קשר',
  'Products' => 'שורת פריט מצוטט',
  'Quotes' => 'הצעת מחיר',
  'Bugs' => 'באג',
  'Project' => 'פרויקט',
  'Prospects' => 'מטרה',
  'ProjectTask' => 'משימת הפרויקט',
  'Tasks' => 'משימה',
  'KBContents' => 'מרכז מידע',
  'RevenueLineItems' => 'שורות פרטי הכנסה',
);