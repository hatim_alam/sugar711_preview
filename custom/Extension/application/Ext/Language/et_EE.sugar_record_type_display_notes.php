<?php
 // created: 2018-01-23 09:18:24

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Võimalus',
  'Tasks' => 'Ülesanne',
  'ProductTemplates' => 'Tootekataloog',
  'Quotes' => 'Pakkumus',
  'Products' => 'Pakkumuse artikkel',
  'Contracts' => 'Leping',
  'Emails' => 'E-kiri',
  'Bugs' => 'Viga',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projekti ülesanne',
  'Prospects' => 'Eesmärk',
  'Cases' => 'Juhtum',
  'Leads' => 'Müügivihje',
  'Meetings' => 'Kohtumine',
  'Calls' => 'Kõne',
  'KBContents' => 'Teadmusbaas',
  'RevenueLineItems' => 'Tuluartiklid',
);