<?php
 // created: 2018-01-23 09:18:15

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Oppgave',
  'Opportunities' => 'Muligheter',
  'Products' => 'Produkt',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Feil',
  'Cases' => 'Sak',
  'Leads' => 'Lead',
  'Project' => 'Prosjekt',
  'ProjectTask' => 'Prosjektoppgave',
  'Prospects' => 'Mål',
  'KBContents' => 'KB-dokumenter',
  'RevenueLineItems' => 'Omsetningsposter',
);