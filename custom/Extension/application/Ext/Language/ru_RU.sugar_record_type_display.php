<?php
 // created: 2018-01-23 09:18:18

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Контрагент',
  'Opportunities' => 'Сделка',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Contacts' => 'Контакты',
  'Products' => 'Продукт коммерческого предложения',
  'Quotes' => 'Коммерческое предложение',
  'Bugs' => 'Ошибка',
  'Project' => 'Проект',
  'Prospects' => 'Адресат',
  'ProjectTask' => 'Проектная задача',
  'Tasks' => 'Задача',
  'KBContents' => 'База знаний',
  'RevenueLineItems' => 'Доходные продукты',
);