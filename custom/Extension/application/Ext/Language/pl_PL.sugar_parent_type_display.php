<?php
 // created: 2018-01-23 09:18:17

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Kontrahent',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Zadanie',
  'Opportunities' => 'Szansa',
  'Products' => 'Pozycja oferty',
  'Quotes' => 'Oferta',
  'Bugs' => 'Błędy',
  'Cases' => 'Zgłoszenie',
  'Leads' => 'Namiar',
  'Project' => 'Projekt',
  'ProjectTask' => 'Zadanie projektowe',
  'Prospects' => 'Odbiorca',
  'KBContents' => 'Baza wiedzy',
  'RevenueLineItems' => 'Pozycje szansy',
);