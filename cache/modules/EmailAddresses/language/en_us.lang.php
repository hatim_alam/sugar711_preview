<?php
// created: 2018-01-23 09:50:01
$mod_strings = array (
  'LBL_EMAIL_ADDRESS_ID' => 'ID',
  'LBL_EMAIL_ADDRESS' => 'Email Address',
  'LBL_EMAIL_ADDRESS_CAPS' => 'Email Address caps',
  'LBL_INVALID_EMAIL' => 'Invalid Email',
  'LBL_OPT_OUT' => 'Opted Out',
  'LBL_DATE_CREATE' => 'Date Create',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_DELETED' => 'Delete',
  'LBL_MODULE_NAME' => 'Email Addresses',
  'LBL_MODULE_NAME_SINGULAR' => 'Email Address',
);