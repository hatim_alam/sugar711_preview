<?php
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */



$sugar_version      = '7.11.0.0-preview.1';
$sugar_db_version   = '7.11.0.0-preview.1';
$sugar_flavor       = 'ENT';
$sugar_build        = '146';
$sugar_timestamp    = '2018-01-06 1:26am';

?>
